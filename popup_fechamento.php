<style>
  h1 {
    text-align: center;
    font-family: Tahoma, Arial, sans-serif;
    color: #06D85F;
    margin: 80px 0;
  }

  .box {
    width: 80%;
    margin: 0 auto;
    background: rgba(255,255,255,0.2);
    padding: 35px;
    border: 2px solid #fff;
    border-radius: 20px/50px;
    background-clip: padding-box;
    text-align: center;
  }

  .overlay {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.7);
    transition: opacity 500ms;
    visibility: hidden;
    opacity: 0;
  }
  .overlay:target {
    visibility: visible;
    opacity: 1;
  }

  .popup_fechamento {
    margin: 120px auto;
    padding: 20px;
    background: #fff;
    border-radius: 5px;
    width: 30%;
    position: relative;
    transition: all 5s ease-in-out;
  }

  .popup_fechamento h2 {
    margin-top: 0;
    color: #333;
    font-family: Tahoma, Arial, sans-serif;
  }
  .popup_fechamento .close {
    position: absolute;
    top: 20px;
    right: 30px;
    transition: all 200ms;
    font-size: 30px;
    font-weight: bold;
    text-decoration: none;
    color: #333;
  }
  .popup_fechamento .close:hover {
    color: #000;
  }
  .popup_fechamento .content {
    max-height: 60%;
    overflow: auto;
  }

  @media screen and (max-width: 700px){
    .box{
      width: 70%;
    }
    .popup_fechamento{
      width: 70%;
    }
  }
</style>

<?php
   //$query = mysql_query("SELECT num_nota_fiscal FROM pde_fato_vendas");
   
  echo '<div id="popup_fechamento" class="overlay">
  	<div class="popup_fechamento">
            <h2 class="ui center aligned header">INSIRA A SENHA</h2>
            <a class="close" href="#">&times;</a>
            <div class="content">
            <br>
            <form method="post" action="cancelar_nota.php">
                <div class="ui fluid input">
                  <input type="password" name="senha" placeholder="senha">
                  <input type="hidden" name="nota" value="'.$ver_hist['num_nota_fiscal'].'">
                  <br>
                </div>
              <br><br>
                  <a class="ui small button" href="#">Voltar</a>
                  <input type="submit" class="ui small right floated red button" value="Avançar">
            </form>
            </div>'
       .'</div>'
     .'</div>';
 ?>

